import React from "react";

class Count extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            count:this.props.init
        }
        //this.onBtnCountClickHandler = this.onBtnCountClickHandler.bind(this);
    }

    onBtnCountClickHandler = () => {
        console.log(this);
        this.setState({
            count: this.state.count + 1
        })
    }

    render() {
        return(
            <div>
                <p>Số lần click: {this.state.count} lần</p>
                <button onClick = {this.onBtnCountClickHandler}>Click me!</button>
            </div>
        );
    }
}

export default Count;
