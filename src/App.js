import Count from "./components/Count";

function App() {
  return (
    <div>
      <Count init = {1}/>
    </div>
  );
}

export default App;
